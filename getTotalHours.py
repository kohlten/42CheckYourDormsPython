#!/usr/bin/python

import time
import datetime
import calendar
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from sys import argv

class TimeInt(object):
	def __init__(self, seconds=0, minutes=0, hours=0):
		self.hours = hours
		self.minutes = minutes
		self.seconds = seconds

	def normalize(self):
		while self.seconds > 59:
			self.seconds -= 59
			self.minutes += 1
		while self.minutes > 59:
			self.hours += 1
			self.minutes -= 59

	def add(self, seconds, minutes=0, hours=0):
		self.seconds += seconds
		self.minutes += minutes
		self.hours += hours
		self.normalize()

	def reset(self):
		self.seconds = 0
		self.minutes = 0
		self.hours = 0

	def to_string(self):
		return (str(self.hours) + ":" + str(self.minutes) + ":" + str(self.seconds))

	def print_vals(self):
		return ("h: " + str(self.hours) + " m: " + str(self.minutes) + " s: " + str(self.seconds))


def get_value():
	options = webdriver.ChromeOptions()
	#options.add_argument('headless')
	
	driver = webdriver.Chrome(chrome_options=options)
	driver.get("https://signin.intra.42.fr/users/sign_in")
	
	elem = WebDriverWait(driver, 30).until(EC.presence_of_element_located((By.NAME, 'user[login]')))
	elem = driver.find_element_by_name("user[login]")
	
	elem.clear()
	elem.send_keys(argv[1])

	elem1 = driver.find_element_by_name("user[password]")
	elem1.clear()
	elem1.send_keys(argv[2])

	elem2 = driver.find_element_by_name("commit")
	elem2.click()

	driver.get('https://profile.intra.42.fr/users/' + argv[1] + '/locations_stats')
	values = driver.find_element_by_xpath("//body").text
	return values

if __name__ == "__main__":
	values = get_value().replace("{", '').replace("}", '').replace('"', '').split(",")
	for i in range(len(values)):
		values[i] = values[i].split(":")
		values[i][1] = values[i][1:]

	values = sorted(values, key=lambda x: x[0])
	total = TimeInt(0, 0, 0)

	for i in range(len(values)):
		for j in range(len(values[i][1])):
			values[i][1][j] = int(values[i][1][j]) 
		v = values[i][1]
		v.reverse()
		total.add(*v)

	print("Total: " + total.to_string())
	total.reset()

	today = datetime.date.today()
	current = (today.weekday() + 1) % 7
        if today.day - current > 0:
	    start = datetime.date(today.year, today.month, today.day - current)
        else:
            start = datetime.date(today.year, today.month - 1, calendar.monthrange(today.year, today.month - 1)[1] - (current - today.day))
        found = False

	for val in values:
		if val[0] == str(start) or val[0] > str(start):
			found = True
		if found:
			v = val[1]
			total.add(*v)
			v = TimeInt(*v)
			print("You had " + v.print_vals() + " hours for a current total of " + str(total.to_string()))

	print("This week: " + total.print_vals())
