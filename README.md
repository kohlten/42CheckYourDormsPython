Python scraper to get current hours for the week on the 42 website. 

To run you need to download the chrome selenium driver at https://chromedriver.storage.googleapis.com/index.html?path=2.38/

Then just run python getTotalHours.py USERNAME PASSWORD